USE cout_likeoprom;
DROP TABLE IF EXISTS Konto, Uzytkownik;

CREATE TABLE  Uzytkownik(
	id int NOT NULL AUTO_INCREMENT,
    	PRIMARY KEY (id),
	nick varchar(30) NOT NULL,
 	pass varchar(41) NOT NULL,
	email varchar(60) NOT NULL,
	last_log datetime
); 


CREATE TABLE Konto ( 
	id int PRIMARY KEY, 
	money numeric(12,4), 
	access BOOLEAN, 
	admin BOOLEAN, 

	CONSTRAINT FK_id FOREIGN KEY (id) REFERENCES Uzytkownik(id) 
);

INSERT INTO Uzytkownik(nick,email,last_log) VALUES('Wojciech','wojciech.balawender@smcebi.edu.pl',curdate());
INSERT INTO Uzytkownik(nick,email,last_log) VALUES('MarcinS','marcin.sciborowski@smcebi.edu.pl',now());
INSERT INTO Uzytkownik(nick,email,last_log) VALUES('NataliaS','natalia.skopinska@smcebi.edu.pl',now());
INSERT INTO Uzytkownik(nick,email,last_log) VALUES('MarcinB','marcin.bys@smcebi.edu.pl',now());

INSERT INTO Konto(id,money,access,admin) VALUES(1,0.0004,1,1);

-- Kampania (akutalne reklamy do "wyklikania" w serwisie)
