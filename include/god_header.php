<?php 
include_once 'include/header.php';
require_once 'fun/session.php'; 
require 'fun/security_mod.php'; //Ale jestem dowcipny :D

if(zalogowany()) 
{
	include("include/userbar.php");
	include_once('include/menu_left.php');
}
else
{
	include("include/guestbar.php");
	include_once('include/guest_menu_left.php');
}

?>