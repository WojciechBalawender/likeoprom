#!/usr/bin/python
# https://www.tutorialspoint.com/python/python_database_access.htm
# Plik wykonujacy administracyjne operacje na kontach: ban, zmiana hasla

#import MySQLdb
#import hashlib

def ban(user_id):
    """ Banowanie uzytkownika po ID (zmiany w tabeli Konto)"""
    db = MySQLdb.connect("sqlhosting5.dt.pl", "cout_likeoprom", "4NatiRanger!", "cout_likeoprom")
    cursor = db.cursor()

    cursor.execute("""
            UPDATE Konto
            SET access='0'
            WHERE id='%s'
    """ %user_id)

    db.commit()
    db.close()

def change_passwd(user_id, new_passwd):
    """ Zmienia haslo uzytkownikowi o podanym ID (zmiany w tabeli Uzytkownik) """
    db = MySQLdb.connect("sqlhosting5.dt.pl", "cout_likeoprom", "4NatiRanger!", "cout_likeoprom")
    cursor = db.cursor()

    salt = '-> #GIT# <-'
    np = hashlib.sha1(new_passwd + salt).hexdigest() #zahashowane nowe haslo

    cursor.execute("""
            UPDATE Uzytkownik
            SET pass='%s'
            WHERE id='%s'
    """ %(np, user_id))

    db.commit()
    db.close()

# ban(15)
# change_passwd('4', 'dziala')
