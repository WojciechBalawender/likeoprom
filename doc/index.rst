.. Likeoprom documentation master file, created by
   sphinx-quickstart on Sun Jan 22 11:22:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Witaj w dokumentacji projektu Likeoprom!
=====================================

Opis projektu
====================================
Strona Likeoprom ma pomagac uzytkownikom w zdobywaniu tzw. like-ow 
w popularnym serwisie spolecznosciowym Facebook.
Uzytkownicy moga wymieniac/kupowac lajki na swoj profil.

Spis tresci:
====================================
.. toctree::
   :maxdepth: 2
   
   tutorial
   license
   code

Zakladki
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

